/**
 *Author: Mashoko Blessing <bmashcom@hotmail.com>
 *Website: mashcom.github.io
 *
 *This file contains all the required functionality for managing posts and pages
 *NB: This is a quick code to create a Minimum Viable Product, this might not 
 *be the best code I have ever written ever!
 */
angular.module('PortalApp',
        [
            'AppRouter',
            'ngSanitize',
            'angular-growl',
            'ngAnimate',
            'chieffancypants.loadingBar',
        ])

        //all request to the api must be directed to this base
        .value('API_BASE', 'https://localhost/laravel 5 tut/public/')

        .controller('PortalController', function ($scope, $http, $location, $sce, API_BASE, growl, cfpLoadingBar) {

            $scope.API_POINTS = {NEWS: 'news', FACULTY: 'faculty', DEPARTMENT: 'department'};

            $http.defaults.headers.put["Content-Type"] = "application/x-www-form-urlencoded";
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

            var transform = function (data) {
                return $.param(data);
            };

            /**
             * Initialise the text editor and select the most appropriate editing mode and post type
             * @returns {undefined}
             */
            $scope.initEditor = function () {

                var post_id = $location.search().id;
                var is_page = $location.search().is_page;
                var dept = $location.search().dept;
                $scope.is_fresh = $location.search().is_fresh;

                $scope.editor_title = "News & Notices Editor";
                $scope.editor_heading = "Title";
                $scope.is_faculty = false;
                $scope.delete_action_target = "news";
                if (is_page == "true") {
                    $scope.delete_action_target = "faculty";
                    $scope.is_faculty = true;
                    $scope.show_dept_panel = true;
                    $scope.editor_title = "Faculty Content Manager";
                    $scope.editor_heading = "Faculty Name";
                }

                if (dept != undefined) {
                    $scope.delete_action_target = "department"
                    $scope.is_dept = true;
                    $scope.show_dept_panel = false;
                    $scope.editor_title = "Faculty Department Content Manager";
                    $scope.editor_heading = "Department Name";
                    is_page = "dept";
                }

                $scope.getPosts(post_id, true, is_page);
                $scope.active_post = post_id;
            };


            $scope.deleteDialogUI = function (delete_id, active_delete_type) {
                $scope.active_delete_id = delete_id;
                $scope.active_delete_type = active_delete_type;
                openModal("#deleteDialogModal");
            }

            /**
             * Determine the appropriate url to use and make a request object
             * @returns {app_L12.$scope.generateEditorUrl.request_object}
             */
            $scope.generateEditorUrl = function () {

                var raw_editor_content = $("#text-area-og").val();
                var is_page = $location.search().is_page;
                var dept = $location.search().dept;
                var is_fresh = $location.search().is_fresh;

                //choose whether we are editing or creating new post
                var target_url = API_BASE + $scope.API_POINTS.NEWS;
                var method_to_use = "POST";

                //this is a faculty page being edited
                if (is_page == "true") {
                    target_url = API_BASE + $scope.API_POINTS.FACULTY;
                    if ($scope.active_post != null && $scope.active_post != undefined) {
                        target_url = API_BASE + $scope.API_POINTS.FACULTY + "/" + $scope.active_post;
                        method_to_use = "PUT";
                    }
                }

                //edit dept
                if (is_page == "true" && dept != undefined) {
                    target_url = API_BASE + $scope.API_POINTS.DEPARTMENT;
                    if ($scope.active_post != null && $scope.active_post != undefined) {
                        target_url = API_BASE + $scope.API_POINTS.DEPARTMENT + "/" + $scope.active_post;
                        method_to_use = "PUT";
                    }
                }

                //add new dept
                if (dept != undefined && is_fresh == "true") {
                    target_url = API_BASE + $scope.API_POINTS.DEPARTMENT;
                    method_to_use = "POST";
                }

                //edit news article
                if ($scope.active_post != null && $scope.active_post != undefined && is_page != 'true') {
                    target_url = API_BASE + "news/" + $scope.active_post;
                    method_to_use = "PUT";
                }

                var request_params = {
                    "heading": $scope.heading,
                    "content": raw_editor_content,
                    "faculty_id": $location.search().faculty,
                    "event_date":$scope.event_date,
                    "event_time":$scope.event_time
                };
                
                console.log(request_params);

                var request_object = {
                    method: method_to_use,
                    url: target_url,
                    data: request_params,
                    transformRequest: transform
                };

                return request_object;

            }

            /**
             *Reload a post after a new save to get the saved post
             */
            $scope.loadAfterNewSave = function (id, is_page, is_dept) {

                $location.url("/editor?is_page=" + is_page + "&id=" + id + "&dept=" + is_dept);
                if (is_dept == undefined) {
                    $location.url("/editor?is_page=" + is_page + "&id=" + id);
                }

                $scope.getPosts(id, true, is_page);

            }

            /**
             * Save changes made through the editor
             * @returns {undefined}
             */
            $scope.create = function () {

                var is_fresh = $location.search().is_fresh;
                var is_page = $location.search().is_page;

                $http($scope.generateEditorUrl())
                        .then(function successCallback(response) {
                            console.log(response.data[0]);

                            if (response.data == "success" || response.data.status == "success") {

                                growl.success('Proccess completed successfully', {title: 'Success!'});

                                if (is_fresh == "true") {
                                    $scope.loadAfterNewSave(response.data.id, is_page, $location.search().dept);
                                }


                            }
                            else {
                                growl.warning('Server could not process the request', {title: 'Oops!'});
                            }

                        }, function errorCallback(response) {
                            console.log(response);
                            growl.error('Server could not be reached, please try again', {title: 'Error!'});

                        });
            }

            //Get the list of saved posts
            $scope.getPosts = function (id = null, edit_mode = false, faculty_page = false) {

                var api_point = $scope.API_POINTS.NEWS;

                //retrieve faculty
                if (faculty_page == "true" || faculty_page == true) {
                    api_point = $scope.API_POINTS.FACULTY;
                }
                if (faculty_page === "dept") {
                    api_point = $scope.API_POINTS.DEPARTMENT;
                }

                if (isNaN(id) == false && id != null) {
                    api_point = api_point + "/" + id;
                }

                $http.get(API_BASE + api_point)
                        .then(function successCallback(response) {
                            $scope.posts = response.data;
                            var post_id = $location.search().id;
                          

                            //edit mode is active us 
                            if (post_id != undefined && edit_mode == true) {

                                $scope.heading = $scope.posts.heading;
                                var post_content = $scope.posts.content;
                                if (faculty_page == "true" || faculty_page === "dept") {
                                    post_content = $scope.posts.description;
                                    $scope.heading = $scope.posts.name;
                                }

                            }
                            $("#sandbox").empty();
                            $("#sandbox").append(post_content);
                            $scope.text_editor = post_content;
                            $(".textarea").wysihtml5();

                        }, function errorCallback(response) {

                        })

                return $scope.posts;
            }


            /**
             *Publish or unpublish a post
             */
            $scope.publish = function (id, action, is_page) {

                var url = API_BASE + "news/publish/news/" + id + "?action=" + action;
                if (is_page == true) {
                    url = API_BASE + "news/publish/page/" + id + "?action=" + action;
                }
                if (is_page == "dept") {
                    url = API_BASE + "news/publish/dept/" + id + "?action=" + action;
                    is_page = true;
                }

                $http.post(url)
                        .then(function successCallback(response) {
                            if (response.data == "success") {
                                //update ui, dont go to server for a refresh. Wasteful!
                                if (action == 0) {
                                    $(".pub-" + id).removeClass('ng-hide');
                                    $(".pub-" + id).show();
                                    $(".pub-" + id).removeAttr('style');
                                    $(".unpub-" + id).addClass('ng-hide');
                                    $(".unpub-" + id).hide();
                                    $(".unpub-" + id).removeAttr('style');


                                } else {
                                    $(".unpub-" + id).removeClass('ng-hide');
                                    $(".pub-" + id).hide();

                                }

                                growl.success('Request executed succesfully', {title: 'Success!'});


                            }
                            else {
                                growl.warning('Server could not process the request', {title: 'Oops!'});
                            }
                        }, function errorCallback(response) {
                            growl.error('Error could not connect to server', {title: 'Oops!'});

                        });
            }


            $scope.flagAsEvent = function (id, action) {
                $http.post(API_BASE + $scope.API_POINTS.NEWS + "/event/flag/" + id + '?action=' + action)
                        .then(function successCallback(response) {
                            if (response.data == "success") {
                                $scope.flagged_as_event = true;
                                growl.success("Post flagged as event", {title: "Event Flagging"});
                            }

                        }, function errorCallback(response) {
                            growl.error('Error could not connect to server', {title: 'Oops!'});

                        })
            }

            $scope.deletePost = function (active_id, url) {
                is_page = false;
                if (url == "faculty" || url == "department") {
                    is_page = true;
                }

                $http.delete(API_BASE + url + "/" + active_id)
                        .then(function successCallback(response) {
                            console.log(response);
                            if (response.data == true) {
                                growl.success('Post deleted sucessfully', {title: 'Success!'});
                                if (url == "department") {
                                    $("#dept-box-" + active_id).remove();
                                } else {
                                    $("#post-box-" + active_id).remove();
                                }

                                closeModal("#deleteDialogModal");
                                return;

                            }
                            else {
                                growl.warning('Server could not process the request', {title: 'Oops!'});
                            }


                        }, function errorCallback(response) {
                            growl.error('Server could not process the request', {title: 'Error!'});

                        });
            };

            $scope.previewMode = function () {
                $scope.edit_mode = false;
                $scope.toggle_preview_mode = true;

            };

            /**
             *Activate the editor
             */
            $scope.editMode = function () {
                $scope.edit_mode = true;
                $scope.toggle_preview_mode = false;

            }
            $scope.editMode();
        })

        .controller('AuthController', function ($scope, $http, $location, $window, $sce, API_BASE, growl, cfpLoadingBar) {

            $scope.login = function () {

                $scope.error_message = undefined;

                var data = {
                    "_csrf_token": $scope.csrf_token,
                    "email": $scope.email,
                    "password": $scope.password,
                    "remember_me": $scope.remember_me
                };

                $http.post(API_BASE + "auth/login", data).then(function successCallback(response) {
                    console.log(response);
                    var req_response = response.data;
                    if (req_response.status == "success") {
                        growl.success("Authentication successfully, Initialising portal", {title: "Success!"});
                        $window.location.href = req_response.redirect_to;
                    }
                    else if (req_response.status == "failed") {
                        growl.warning("Authentication failed, please try again", {title: "Warning!"});
                        $scope.error_message = req_response.message;

                    }
                    else {
                        growl.error("Server could not respond properly, reloading", {title: "Error!"});
                        $window.location.href = req_response.redirect_to;

                    }

                }, function errorCallback(response) {
                    growl.danger("Server could not be reached", {title: "Error!"});

                });
            }
        })

        .controller('UserController', function ($scope, $http, $location, $sce, API_BASE, growl, cfpLoadingBar) {

            $scope.getUsers = function () {

                $http.get(API_BASE + "users").then(function successCallback(response) {
                    $scope.users = response.data;
                }, function errorCallback(response) {
                    growl.error("Server could not be reached, please try again", {title: "Error!"})
                })
            }
            
            
            $scope.update = function (id, column, value) {


                $http.put(API_BASE + "users/" + id, {"id": id, "status": value})
                        .then(function successCallback(response) {

                            console.log(response.data)
                            if (response.data === "success") {
                                growl.success("Update successfully executed", {title: "Success"});
                                $scope.getUsers();
                            } else {
                                growl.warning("Update could not be executed", {title: "Warning"});
                            }

                        }, function errorCallback(response) {

                        })
            }

            $scope.createUser = function () {

                $http.post(API_BASE + "users", {"name": $scope.name, "email": $scope.email, "password": $scope.password, "role": $scope.role})
                        .then(function successCallback(response) {
                            console.log(response.data)
                            if (response.data == "success") {

                                growl.success("User created successfully", {title: "Success!"});
                                $scope.getUsers();
                                closeModal("#newUserDialogModal")
                            }
                            else {
                                growl.warning("User account could not be created", {title: "Ooops!"})

                            }
                        }, function errorCallback(response) {
                            $scope.form_errors = response.data;
                            $scope.errors_count = Object.keys($scope.form_errors).length;

                        })
            }
        })

