/**
 * Routing of the application is all handled here
 */
angular.module("AppRouter", ['ngRoute', 'PortalApp']);

angular.module("AppRouter").config(function ($routeProvider, $locationProvider) {

    $routeProvider.when('/', {
        controller: "PortalController",
        templateUrl: "partials/index.html"
    }).when('/editor', {
        controller: "PortalController",
        templateUrl: "partials/editor.html"
    }).when('/posts', {
        controller: "PortalController",
        templateUrl: "partials/posts.html"
    }).when('/faculty', {
        controller: "PortalController",
        templateUrl: "partials/pages.html"
    }).when('/users', {
        controller: "UserController",
        templateUrl: "partials/users.html"
    }).when('/404', {
        controller: "PortalController",
        templateUrl:"partials/404.html"
    }).otherwise({
        redirectTo: '/404'
    });

});
