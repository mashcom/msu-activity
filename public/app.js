angular.module('PortalApp',
        [
            'app',
            'AppRouter',
            'ngSanitize',
            'ngAnimate'
        ])

        .value('API_BASE', "http://localhost:8080/planner/")

        //basic application init
        .controller('HeaderController', function ($scope, $http, API_BASE) {
            //get profile pic
            $http.get(API_BASE + "api/user/get_avatar?avatar").then(function successCallback(response) {
                $scope.user_avatar = response.data;
            }, function errorCallback(response) {

            });

            //get all user information
            $http.get(API_BASE + "api/user/get_avatar").then(function successCallback(response) {
                $scope.logged_user = response.data;
            }, function errorCallback(response) {

            });

            init_ui();
        })
