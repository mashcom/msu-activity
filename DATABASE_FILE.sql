-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2018 at 08:38 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `msu`
--

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
`id` int(10) unsigned NOT NULL,
  `faculty_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `is_published` enum('0','1') COLLATE utf8_unicode_ci DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `faculty_id`, `name`, `description`, `is_published`) VALUES
(2, 0, 'HINFO', '', '0'),
(3, 0, 'DSSDSDSDSDS', '', '0'),
(4, 0, 'slsks', '', '1'),
(7, 7, 'Telecommunications', '', '1'),
(8, 7, 'Computer Science & Information Systems', '', '1'),
(9, 7, 'Physics', '', '1'),
(10, 4, 'Banking & Finance', '', '1'),
(14, 0, 'lskdlsk', '', '0'),
(15, 19, 'me de', '', '0'),
(16, 11, 'Culture', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `faculties`
--

CREATE TABLE IF NOT EXISTS `faculties` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `is_published` enum('0','1','','') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `faculties`
--

INSERT INTO `faculties` (`id`, `name`, `description`, `is_published`, `updated_at`, `created_at`) VALUES
(4, 'Commerce', '<p>\n\n</p><p><strong></strong></p>\n\n<p>\n\n<img width="895" alt="" src="https://localhost:8000/wp-content/uploads/2018/01/Online-Ecommerce-Customer-Small-1024x585.png" height="511" title="Image: https://localhost:8000/wp-content/uploads/2018/01/Online-Ecommerce-Customer-Small-1024x585.png"><p>The faculty of Commerce is the lead and largest faculty at Midlands State University which is located at the Graduate School of Business leadership (GSBL) and faculty of Law campus.</p><p>Whilst MSU enjoys the general mandate at law, of offering any degree programmes with resources permitting, over the years the university has devoted a larger chunk of its resources to the faculty of Commerce and the faculty has distinguished itself as the faculty of choice which was established for the general advancement of knowledge in the field of Commerce and industry and more particularly promoting that species of knowledge that distinguishes the field of commerce from others.</p><p>It is worth noting that out GSBL is the only Graduate School in Zimbabwe which offers internationally accredited MBAs and EMBAs with the Association of African Business Schools (AABS). The association has reciprocal partnerships with: the Association of MBAs (UK), The Association of Indian Management Schools (AIMS), the Association to Advance and Collegiate Schools of Business (AACSB) USA, the Accreditation council of Business Schools and Programmes (ACBSP) USA, the Central and Eastern European Management Association (CEEMAN) (Eastern Europe) among others. This has given the faculty a strategic advantage in its endeavour to embed itself in providing global quality education in dynamic markets.</p><p>The faculty underscores the importance of commercializing research as a spring board of economic development and currently is also on a resource mobilisation drive which has seen the opening of weekend school for our various masters and visiting school programmes in Harare in line with our faculty motto, “<em>World class excellence is our culture and share the knowledge with a Havruta”.</em>&nbsp;The faculty is in the process of accrediting all our Master of Commerce programmes with the ACBSP and CEEMAN as we take the strategic global route. By the end of 3 years our Mcom programmes will be globally competitive when accredited.</p><p><strong>Departments in the faculty:</strong></p><ul><li>Accounting</li><li>Banking &amp; Finance</li><li>Business Management</li><li>Economics</li><li>Marketing Management</li><li>Retail and Logistics management</li><li>Tourism and Hospitality management</li><li>Centre for Entrepreneurship and Innovation</li></ul><p><strong>Schools</strong></p><p><a target="_blank" rel="nofollow" href="http://ww4.msu.ac.zw/graduateschool">The Graduate School of Business Leadership</a></p>\n\n<br></p><p></p>', '1', '2018-01-09 14:24:12', '2017-12-22 10:20:04'),
(7, 'Science & Technology', '<p>\n\n</p><p><strong>Faculty Overview</strong></p><p><strong>\n\n<img alt="" src="https://localhost:8000/wp-content/uploads/2018/01/science.jpg" title="Image: https://localhost:8000/wp-content/uploads/2018/01/science.jpg">\n\n<br></strong></p><p>Welcome to the Faculty of Science and Technology at Midlands State University, where our vision of being the leading faculty nationally and globally drives our academic endeavor. This proud faculty has an average enrollment of 1500 students in several departments namely, Computer Science, Applied Physics, Surveying and Geomatics, Food Science and Nutrition, Information Systems, Applied Biosciences and Biotechnology, Chemical Technology , Applied Statistics and Mathematics, 90 teaching staff and graduates on average 350 students annually. In order to ensure that students can achieve their full academic potential the faculty offers these programmes at both undergraduate and post graduate levels to both local and international students. We have over the years rebranded our programmes to suit expectations of industry and in line with the University’s Zimbabwe Agenda for Sustainable Socio-Economic Transformation (ZIMASSET) complaint strategic plan, the faculty strives to be an important centre of research through establishing ties with other institutions of higher learning in Africa and elsewhere in the world.</p>\n\n<br><p></p>', '1', '2018-01-09 14:35:25', '2017-12-26 14:23:01'),
(11, 'Arts', '<p>\n</p><p>Welcome to the Faculty of Arts at Midlands State University whose \nproud history started off as the Faculty of Arts &amp; Social Sciences \nat the University’s inception. Since then, the Faculty of Arts has \nacademically grown in leaps and bounds to boast of nine Departments \ninclusive of a Communication Skills Centre.All these Departments are \nmanned by an exceptionally competent, hardworking and committed staff \nwhose sterling and wonderful job stands at the very heart of Midlands \nState University’s mission in producing innovative and enterprising \ngraduates.</p>\n<p>The Faculty of Arts has a stimulating range of flexible four-year \nundergraduate degree programs and offers Post-Graduate programmes suited\n to students’ own needs and interests. Through rebranding and its \ncontinual adapting to improving on its programme delivery, this has \ngenerated significant knowledge spill-overs resulting in enhanced \neconomic development. Since universities are key drivers of cultural, \npolitical and socio-economic development through the creation of human \ncapital, the Faculty of Arts’ succinct sense of progress encapsulates a \nfocus on creative problem solving, cultural awareness, effective \ncommunication, ethical reasoning, critical analysis, amongst a barrage \nof skills that we impart. It supports and pursues the University’s goal \nof excellence in research, encouraging research of national (in line \nwith Zim Asset) and international standing, and identifying and \nenhancing fields of basic, strategic and applied research. The Faculty \nof Arts &nbsp;therefore offers an exciting and innovative pathway for the \nhumanities enthusiasts to display their executive potential as is \nenriched by its teaching methodologies. Our programmes equip students \nwith highly transferable and flexible skill-set that creates and fosters\n imaginative, courageous and sensitive leaders of tomorrow.</p>\n\n<br><p></p>', '1', '2018-01-07 03:40:40', '2018-01-03 15:06:04'),
(12, 'Mining and Mineral Processing Engineering', '<p>\n\n</p><p><strong>\n\n<img alt="" src="https://localhost:8000/wp-content/uploads/2018/01/mining-1.jpg" title="Image: https://localhost:8000/wp-content/uploads/2018/01/mining-1.jpg">\n\n<br></strong></p><p><strong>Undergraduate Programmes</strong></p><ul><li>Bachelor of Engineering(Honours) Degree in Metallurgical Engineering (5 years)</li><li>Bachelor of Engineering (Honours) Degree in Materials Engineering (5 years)</li><li>Bachelor of Engineering (Honours) Degree in Mining Engineering (5 years)</li><li>Bachelor of Science (Honours) Degree in Geology (4 years)</li></ul><p><strong>Visiting School Programmes (4 years)</strong><br>A Visiting School is envisaged for the Faculty of Mining Sciences at MSU Zvishavane Campus to cater for working professionals and technicians who cannot attend conventional lectures but wish to upgrade their qualifications through specially designed visiting lecture schedules.All the four (4) degree programmes cited above will eventually, in addition to the conventional programme, be offered via Visiting School which is planned to commence 2016.</p>\n\n<br><p></p>', '1', '2018-01-09 14:31:26', '2018-01-06 15:56:02'),
(13, 'Natural Resources Management And Agriculture', '<p>\n\n<img alt="" src="https://localhost:8000/wp-content/uploads/2018/01/CSES-COA-homepage-header-1024x468.jpg" title="Image: https://localhost:8000/wp-content/uploads/2018/01/CSES-COA-homepage-header-1024x468.jpg">\n\n<br></p><p>\n\nThe diversity and synopses of programmes in the Faculty of Natural Resources Management and Agriculture are a product of intensive and regular consultations with key stake holders whose major goal is to enhance the quality of the both under and post graduate studies in various aspects agricultural production against backgrounds of fragile and low natural resource production potential and climate variability.\n\n<br></p>', '1', '2018-01-09 14:32:39', '2018-01-06 15:56:38'),
(14, 'Social Sciences', '<p>\n\n</p><div><h2><img alt="" src="https://localhost:8000/wp-content/uploads/2018/01/Social-Science-1024x602.jpg" title="Image: https://localhost:8000/wp-content/uploads/2018/01/Social-Science-1024x602.jpg" style="font-size: 14px; font-weight: normal;"><br></h2></div><div><div><p>The Faculty of Social Sciences is a place where ideas meet practice. The Faculty considers itself to be the heart and soul of the university. The vision of the Faculty of Social Sciences reads: <strong>“<em>To be a leading unit within the University that promotes academic excellence in teaching, training, research, university service and community engagement</em></strong>. In Social Sciences we study the mind and body and even attempt to influence the soul (through music). We study the human being at three different levels- individual (HPSY, SW HPS), group/ community (LGS, MBTM, SW, MSS, HRM, HGES, PPM, HPSY, HSOC, MCP, MSHE, MHRM, MLGS) and national and international levels (MSIA, HPS). The Faculty of Social Sciences offers diverse degree programmes from Geography and Environmental Studies, to Human Resource Management, Local Governance Studies, Media and Society Studies, Music Business Technology and Musicology, Politics and Public Management, Peace studies, Psychology, Sociology and Social Work</p><p>Pursuant to this vision, the Faculty constantly monitors developments in its environment and in the world at large.  This provides it with societal trends and changes both at home and abroad.  The awareness gained enables the Faculty to respond to trends and changes by introducing new academic programmes or rebranding existing ones to meet the current demand. To this end, the Faculty recently established the School of Social Work located in Harare.</p><p>The Faculty of Social Sciences collaborates with other institutions and has Associate / Affiliate relationships with institutions such as Kushinga Phikelela with the Department of Local Governance Studies as well as College of Creative Arts with the Department of Music Business, Musicology and Technology.</p><p>The University has several collaborative links with other institutions across Africa. It is our intention as a Faculty to actively seek collaboration with more institutions around the globe in the future. With close to 10 000 students, the Faculty of Social Sciences is one of the largest faculties at the Midlands State University. The Faculty comprises 7 vibrant academic departments and a School of Social Work located in three main campuses of: <strong><em>Gweru, Zvishavane and the Harare</em></strong>.</p><p>The Faculty of Social Sciences is always striving to improve the contents of its programes as well as the methods of delivery.  This spirit is embodied in our faculty of prominent and well cited scholars. Their commitment to incomparable teaching and ground breaking research brings intellectual rigor as well as hands-on experience into our classrooms.</p></div></div>\n\n<br><p></p>', '1', '2018-01-09 14:38:35', '2018-01-06 15:58:49'),
(15, 'Education', '<p>\n\n<img width="895" alt="" src="https://localhost:8000/wp-content/uploads/2018/01/education.jpg" height="362" title="Image: https://localhost:8000/wp-content/uploads/2018/01/education.jpg"><p>The Faculty of Education’s mandate is to produce high quality graduate teachers to staff schools in Zimbabwe and beyond. The Faculty’s three (3) different modes of entry ensure that all clients requiring teacher education manage to enter into the Faculty’s degree programmes. The pre-service mode of entry is a post A Level eight semester programme. The in-service programme is divided into six (6) semesters for block release and four (4) semesters for full time students. The Faculty of Education is made up of five (5) departments namely: Adult Education, Applied Education, Educational Foundations, Management and Curriculum Studies, Educational Technology and Gender Studies.</p>\n\n<br></p>', '1', '2018-01-09 14:27:10', '2018-01-06 16:00:08'),
(16, 'Law', '<p>\n\n<img width="895" alt="" src="https://localhost:8000/wp-content/uploads/2018/01/Law-1024x512.jpg" height="448" title="Image: https://localhost:8000/wp-content/uploads/2018/01/Law-1024x512.jpg"><div><div><div><div><div><div><div><p><strong>Faculty Overview</strong></p><p>The Midlands State University Law School is located in Gweru in the Midlands Province of Zimbabwe. It is situated approximately 5 kilometers from the City Centre. It is located at the Graduate School of Business Leadership, Faculty of Law and Faculty of Commerce Campus. The Faculty averages a student enrolment of 30 students per intake. Designated by the Council for Legal Education on the 7th of March 2007, the Midlands State University Law School marks an important chapter in Zimbabwean legal history as it is the first law school to be successfully established in post-independence Zimbabwe. Driven by our motto, <strong>Pioneering for a better legal frontier,</strong>we have cultivated a culture of hunger for more knowledge as we believe that knowledge is power. Our wide network is made possible, by our enterprising Vice Chancellor, and teaching staff who hail from diverse legal backgrounds. We have ingrained this culture in our alumni who are ever widening their circle of influence while advertising their cradle on the regional and international arena.</p><p>&nbsp;</p><p><strong>The Faculty currently offers the following programmes:</strong></p><ul><li>Bachelor of Laws Honours degree (LLBS)</li><li>Master of Laws in Constitutional and Human Rights Law( LLM) with various areas of specialization</li><li>Master of Philosophy in Law</li><li>Doctor of Philosophy in Law</li></ul></div></div></div></div></div></div></div>\n\n<br></p>', '1', '2018-01-09 14:28:57', '2018-01-06 16:03:29'),
(17, 'Medicine', '<p>\n\n<img width="895" alt="" src="https://localhost:8000/wp-content/uploads/2018/01/medicine-1024x512.jpg" height="448" title="Image: https://localhost:8000/wp-content/uploads/2018/01/medicine-1024x512.jpg"><div><p><strong>Faculty Overview</strong><br></p><p>&nbsp;Welcome to the Faculty of Medicine, the newest faculty at the Midlands State University. After approval from the Medical and Dental Practitioners Council of Zimbabwe (MDPCZ), the faculty enrolled its first cohort of twenty students, eight females and 12 males, at the end of February 2016.  This small number allows for an excellent student – staff ratio. The faculty currently has one programme, the five-year undergraduate Bachelor of Medicine and Bachelor of Surgery (MB ChB) which follows the UZ regulations and curriculum.&nbsp;</p><p>The programme focuses on the foundations of medicine including Anatomy, Physiology, Biochemistry and Behavioural Sciences, as well as intensive clinical training in all disciplines including Medicine, Anaesthesia, General Surgery and Obstetrics &amp; Gynaecology. The faculty has clinicians forming the nucleus that is working with the Medical Superintendent of Gweru Provincial Hospital, to transform the public hospital into a teaching hospital. The faculty is highly indebted to the medical fraternity in the Midlands Province for their support. The introduction of the faculty will greatly enhance the provision of high quality public health and clinical services in the Midlands Province.</p></div><div><div><div><div><div><div><div><h3><a target="_blank" rel="nofollow" href="http://ww4.msu.ac.zw/medicine/#programmes-in-the-faculty" title="Link: http://ww4.msu.ac.zw/medicine/#programmes-in-the-faculty">Programmes in the Faculty</a></h3><div><div><div><p><strong>Undergraduate Programmes</strong><br>Bachelor of Medicine<br>Bachelor of Surgery (MBChB)</p><p><strong>Entry Requirements</strong><br>1. Passes at Ordinary Level (or approved equivalent) in English Language and Mathematics.<br>2. Passes at Advanced Level (or approved equivalent) in Chemistry and any two of the following three subjects:</p><ul><li>Biology (or Zoology)</li><li>Mathematics</li><li>Physics</li></ul><p>The subject not offered at A Level must normally have been passed at ‘O’ Level.</p></div></div></div></div></div></div></div></div></div></div>\n\n<br></p>', '1', '2018-01-09 14:30:02', '2018-01-06 16:03:46');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_06_25_150906_create_session_table', 1),
('2017_12_19_163528_news', 2),
('2017_12_19_164456_faculties', 2),
('2017_12_19_164620_departments', 2);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
`id` int(10) unsigned NOT NULL,
  `heading` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `is_published` enum('0','1','','') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `is_event` int(11) DEFAULT '0',
  `event_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `heading`, `content`, `is_published`, `is_event`, `event_date`, `event_time`, `created_at`, `updated_at`) VALUES
(19, 'President Caps 6 612 Graduates', '<p>\n\n</p><h4></h4>\n\n<img alt="" src="https://localhost:8000/wp-content/uploads/2018/01/website_grad_2017.jpg" width="895" height="597"><p>His Excellency, the President of the Republic of Zimbabwe, and Chancellor of the Midlands State University, Cde Emmerson Dambudzo Mnangagwa, on Saturday the 2nd of December 2017 conferred degrees to a record 6 612 graduates at a lively and colourful ceremony held in the University’s Gweru Main Campus.</p><div><p>Out of the graduating total, 5 429 graduated with first degrees, 1 176 with Masters Degrees, while 7 attained DPhil degrees. In line with national efforts to industrialize and modernize the economy, of note was the fact that, of the graduating total 1 583 graduates were from Science, Technology, Engineering and Mathematics (STEM) programmes. Additionally, it was also the first time that pioneering students in Chemical and Mineral Processing Engineering, Mining and Mineral Processing Engineering, Applied Biosciences and Biotechnology, Biostatistics and the Master of Science in Epidemiology were graduating.</p><p>Speaking at this historic 17th Congregation of the Midlands State University’s graduation ceremony, Midlands State University’s Acting Vice-Chancellor, Professor, Victor Ngonidzashe Muzvidziwa said, driven by its strategic plan and informed by national aspirations, MSU in all its activities ultimately seeks to contribute to the country’s economic development.</p><p>‘All our achievements and future plans as the leading institution of higher learning are informed by our dynamic strategic planning, which is aimed at contributing to the industrialization and modernization of our national economy’, said Professor Muzvidziwa.</p><p>As Midlands State University continues to grow, the Acting Vice-Chancellor also took the opportunity to highlight some of the University’s recent accomplishments and future plans, which included among other things recent construction projects as well as plans to establish another campus.</p><p>‘Plans are afoot to relocate our Faculty of Law and that of Natural Resources Management and Agriculture to the Kwekwe Campus. This envisioned campus will accommodate institutes and centres in specialized fields of Law …. the campus is intended to double up as a research centre, which focuses on small grains and new crops adapted to climate change such as quinoa grain and amarandas’, noted Professor Muzvidziwa,</p><p>In his closing remarks, the Acting Vice-Chancellor congratulated graduands and urged them to be unyielding and to never give up. He further impressed on them the need to be good brand ambassadors, who contribute positively to their communities.</p><p>‘My final word to you our shining graduands is that in line with our time-tested motto, “our hands, our minds our destiny”, as you leave MSU we expect you to be resolute, respectful, courageous and persistent in all your endeavors and to treat life, not as a single sprint but a marathon to be won’, said Professor Muzvidziwa.</p><p>The graduation ceremony was graced by various guests among them, the Permanent Secretary for Higher and Tertiary Education, Science and Technology Development, Professor Francis Gudyanga, MSU Pro-Vice-Chancellors, Professor Kadmiel Wekwete and Professor Doreen Moyo, Acting Vice-Chancellor of the MSU-Manicaland State University of Applied Sciences, Professor A Chawanda, Minister of State in the Midlands Province, the incoming Minister of State for Provincial Affairs in the Midlands Province, Honourable Cde Owen Ncube, Vice-Chancellors from sister universities, members of the University Council and Senate, captains of industry, parents and guardians as well as members of the University Community.</p></div>\n\n<br><p></p>', '1', 0, '11/11/2018', '09:00', '2017-12-25 05:40:20', '2018-01-09 15:05:29'),
(20, 'Arts still available', '<p>\n\n</p><p><strong>Undergraduate Degree</strong><br>Ba African Languages And Culture Honours Degree<br><strong>Masters Degree</strong><br>Master Of Arts In African Languages And Culture Degree</p><p><strong>Department of African Languages and Culture</strong></p><p><strong>Guiding Philosophy and Objective</strong></p><p>The guiding philosophy of the Department of African Languages and Culture at Midlands State University is that African languages are rich store houses of the African people’s social consciousness and their practical-cum-cultural apprehension of the world. The major objective of the department is to bring to the limelight the potential contribution of African languages, African Culture, worldview, values, and philosophy of life to the development of the African society and the world at large.</p><p>The department hopes to achieve this vision through the study of and research in:</p><p>African oral literature in all its diverse forms.<br>Contemporary African literature in written form.<br>Creative writing skills with major focus on children’s literature.<br>Comparative African Linguistic Structures.<br>The Dynamics of African orthographies (writing systems).<br>Indigenous African language translations and lexicographers (the art and craft of dictionary making).<br>The place of traditional and contemporary African culture in the wider spectrum of global socio-economic development.<br>What former students say about the programmes:</p><p>Contrary to the general perception by prospective and current students, this is a very marketable programme with diverse career options; my experience has proved that with this programme, the classroom is not an absolute destiny is you have vision. Marcyline Masimba Chirochierwa (B.A Hons African Languages and Culture- 2002-2006). She is now District Supervisor with CARE International Zimbabwe in Masvingo.</p><p>“The department has got exceptionally, well organized honours and masters programmes with up to date set of modules which provides a perfect balance of theory and practice ultimately produces versatile graduands who can easily cross disciplines both in occupation and the academia.” Phillip Mpofu (B.A 2002-2006, M.A in African Languages and Culture- 2007-2008). He is now a lecturer in the African Languages and Culture Department at the Midlands State University.<br><strong>Programmes</strong></p><p><strong>Undergraduate</strong></p><p><strong>Bachelor of Arts in African Languages and Culture</strong></p><p>The department of African Languages and Culture, in line with the Mission Statement of the Midlands State University whose catch words are “ Our hands, Our minds, Our destiny” seeks to produce a graduate who:</p><p>Will be able to fuse together theoretical concepts and practical applications<br>Will be able to recognize the dignity of labour by utilizing the hands-on-approach in solving the pressing challenges in society.<br>Will be an entrepreneur and an employer rather than an employee.<br>A producer of new ideas geared towards restoring the dignity and value of indigenous African languages and the cultural values they carry.<br>Will be able to define not only his/her destiny but also that of his /her fellow countrymen by cherishing the African value systems, which values can free the African Society from the tentacles of neo- colonialism.</p><p><strong>Admission Requirements:</strong><br>Generally, it is those students who would have studied a particular language at advanced level who will be accepted for undergraduate studies except in situations where the applicant is accepted for undergraduate studies accepted on mature or other special entry. While there may be no hard and fast rules as the other subjects that a prospective student may take at advanced level it is desirable and to the best of interest of the student that Arts subjects are taken. The programme equips students with linguistic and cultural theories to tackle problems.</p><p><strong>Career Prospects</strong></p><p>Editorial fields in publishing Houses and Media organizations<br>Communication/Cultural officers, policy makers in Government Departments and Non-Governmental Organizations<br>Translators/Interpreters e.g. Courts of Law<br>Theatre artists, producers, actors, scriptwriters in the theatre and film industries.<br>Broadcasters for radio and television<br>Teachers in various disciplines<br>Officers in speech therapy services or any other Government Departments<br>Facilitators in non-governmental organizations<br>Ethnography</p><p>Postgraduate</p><p><strong>Master of Arts in African Languages and Culture</strong></p><p>The M.A Degree in African Languages and Culture is an 18-month programme offered on a full-time basis. It is offered on part-time basis for a period of two years. It intends to equip students with an advanced understanding of the African linguistic and cultural landscape. The department believes that unless African languages and cultures are promoted meaningfully, development will remain elusive in Zimbabwe. The programme aims to equip students with relevant linguistic and cultural theories to tackle practical problems in this area in Africa. The major objective of this programme is to produce students who will have the practical know-how and skill to advance African languages and culture in a meaningful way.</p><p><strong>Entry requirements</strong></p><p>Applicants to M.A in African Languages and Culture programme should normally hold an Honours degree in a relevant field (African Languages, Linguistics, and Cultural Studies) with at least Upper Second (2.1) pass from this university or any other recognized university. Lower Second (2.2) passes may be considered for mature entry.<br><strong>Career prospects</strong></p><p>Academia- lecturers, research fellows etc<br>Editorial fields in publishing Houses and Media organizations<br>Communication/Cultural officers, policy makers in Government Departments and Non-Governmental Organizations.<br>Translators/Interpreters e.g. in tourism industry, embassies, Courts of Law etc</p>\n\n<br><p></p>', '1', 0, '', '', '2017-12-25 05:44:22', '2018-01-09 15:05:47'),
(21, 'Breaking News: MSU Crowned 2017 Pan-African Universities Debate Championship (PAUDC) Champions', '<p>\n\n<img alt="" src="https://localhost:8000/wp-content/uploads/2018/01/25394662_1462667827164181_1749077794746925568_o-1024x683.jpg" width="895" height="597"></p><div><p>In yet another historic achievement for both the University and Zimbabwe, Midlands State University’s debate team affectionately known as the Great Debaters made history by becoming the first-ever Zimbabwean team to be crowned as the Pan-African Universities Debate Championship (PAUDC) champions.</p><p>Having hosted the tournament last year (2016), this year, Midlands State University travelled to the University of Buea, in Cameroon where they took on more than 60 teams from over 12 African countries at the 10th edition of the continental debate showcase. Team MSU comprising of Minehle Nzana and Tanaka Chitongo distinguished themselves well, displaying exceptional debate and intellectual prowess to win the competition. To cap off an already historic achievement, Nzana was also awarded the Best Country Speaker award for Zimbabwe.</p><div><p>The competitions ran from 7 to 15 December 2017 under the theme ‘Debate, the 21st Century Alternative to Violence’.</p><p>On behalf of the entire University community to the Great Debators, we say Makorokoto! Congratulations! Amhlope!</p></div></div>\n\n<br><p></p>', '1', 0, '', '', '2018-01-06 15:52:42', '2018-01-09 15:05:16'),
(22, 'Christmas Party', '<p>All former student are invited to a party to be held on 22 December at 15:00 at Main Campus<br></p>', '1', 1, NULL, NULL, '2018-01-07 04:41:27', '2018-01-07 04:41:50'),
(23, 'Carreer Day', '<p>Come meet us at ZITF on 25 April 2018 and get to know how Midlands State University can help you archive your career goals</p><p><br></p><p>updated<br></p>', '1', 1, '12/12/2018', '19:00', '2018-01-07 04:43:51', '2018-01-09 15:15:02');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('bmashcom@hotmail.com', '8de2429da0f94f20a1ee16f3daeea70928a2fc8b30f23e9ea3250d1c1dd5a077', '2017-12-24 07:21:33'),
('fastso@hotmail.com', '063354553d1fe508d03c41d47a371279f8790dbeba8e469c5c40b8a49e961daf', '2017-12-26 11:59:49');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `role`, `status`) VALUES
(1, 'Blessing Mashoko', 'bmashcom@hotmail.com', '$2y$10$a4SRi9ucahYjKPO/Xs5E.uwKIJYO3vI0ddveras3C4GoBuua4r0qe', 'ElHuF5oWf7RvXYykgWKPEkm4o1qYljRiMOE2iybB3ko4ssGEmEl1c5GKKByJ', '2017-12-20 14:44:55', '2018-01-10 20:13:51', 'Adminstrator', NULL),
(2, 'Zlatan Ibrahimovic', 'zlatanshots@gmail.com', '$2y$10$SBCVvKakW7La0xqRvX2MweEUOfXkP5T9ZSRGPCS7u2QuZA2UBBLtC', 'GfUWgiBz65hK7p4Fkt6xXqq3l4yJZih55CzBygjX5slo0fjn5xfR29As31GB', '2017-12-26 04:25:50', '2017-12-26 11:58:43', 'Standard', 'activated'),
(3, 'Alexis Sanchez', 'alexis@gmail.com', '$2y$10$.ZR38OjBNl/I3K.h2LRCTufu3iMWz1Ryjjm2Bc/J.cygdA2tVzfhG', NULL, '2017-12-26 04:27:42', '2017-12-31 17:28:21', 'Standard', 'activated'),
(4, 'Thomas Muller', 'thomas@gmail.com', '$2y$10$Eybgsilxn8woSR9vw7Ij7.ME/npQLvOksI3wcvNtaP6bmvHkX7dt2', NULL, '2017-12-26 04:28:36', '2017-12-26 04:28:36', 'Adminstrator', ''),
(5, 'Christopher Anderson', 'chris@hotmail.com', '$2y$10$MmvqWk4CJuWvWsecVFVZfuPv7X9hA1x9d5/0tq5PlDAL2IwoAMnrq', NULL, '2017-12-26 04:30:11', '2017-12-26 04:30:11', 'Standard', ''),
(6, 'Farai Mashoko', 'fastso@hotmail.com', '$2y$10$4oRzff7IIFvfwsFTN3jZ5OWRtWjuXNTVbrtIK6.venB4VqAaG5Z4e', NULL, '2017-12-26 04:33:00', '2017-12-26 04:33:00', 'Standard', ''),
(7, 'blessing mashoko', 'skajkj@jjkd.com', '$2y$10$GFp1rUqaLHalqvla09HchOkg8i2QxqAWlHSD9gAwD6ioUG7..ImHq', NULL, '2017-12-26 10:51:35', '2017-12-26 10:51:35', 'Standard', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculties`
--
ALTER TABLE `faculties`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
 ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `faculties`
--
ALTER TABLE `faculties`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
