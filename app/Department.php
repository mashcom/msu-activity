<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model {

    public $timestamps = false;

    public function faculty() {
        return $this->belongsTo('App\Faculty');
    }

}
