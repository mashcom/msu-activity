<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get("/", ['as' => 'home', 'uses' => 'SiteController@index']);

Route::get('/notice', ['as'=>'notice','uses'=>'SiteController@getAllNotices']);
Route::get('/notice/{id}', 'SiteController@getNotice');
Route::get('/faculties/{id?}', ['as' => 'faculties', 'uses' => 'SiteController@getFaculties']);
Route::get('/faculty/dept/{id}',['as'=>'department','uses'=>'SiteController@getDepartment']);

Route::get('/home', function() {
    return redirect('/portal/');
});

Route::post('auth/login','Auth\AuthController@authenticate');
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::group(['middleware' => ['auth']], function () {

    Route::resource("news", 'NewsController');
    Route::resource("users", 'UserController');
    Route::resource("department","DepartmentController");
    Route::post("news/publish/page/{id}",'FacultyController@publish');
    Route::post("news/publish/news/{id}",'NewsController@publish');
    Route::post("news/event/flag/{id}",'NewsController@flagAsEvent');
    Route::post("news/publish/dept/{id}",'DepartmentController@publish');
    Route::resource("faculty", 'FacultyController');
    Route::get('portal', function() {
        return view('portal.home');
    });
    
    
});
