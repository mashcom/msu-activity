<?php

namespace App\Http\Controllers;

use App\Department;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class DepartmentController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        $Department = new Department();
        $Department->name = $request->heading;
        $Department->description = $request->content;
        $Department->faculty_id = $request->faculty_id;

        if ($Department->save()) {
            return json_encode(array("status" => "success", "id" => $Department->id));
        }
        return "failed";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        return Department::with("faculty")->findOrFail($id)->toJson();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request) {
        $Department = Department::findOrFail($id);
        $Department->name = $request->heading;
        $Department->description = $request->content;

        if ($Department->save()) {
            return "success";
        }
        return "failed";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        return Department::destroy($id);
    }

    public function publish($id, Request $request) {
        $Department = Department::findOrFail($id);
        $Department->is_published = $request->action;
        if ($Department->save()) {
            return "success";
        }
        return "failed";
    }

}
