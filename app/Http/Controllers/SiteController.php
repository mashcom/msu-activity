<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\News;
use App\Faculty;
use Illuminate\Http\Request;

class SiteController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $news = News::where('is_published', '1')->where('is_event',0)->get();
        $events = News::where('is_published', '1')->where('is_event',1)->get();
        $faculties = Faculty::where('is_published', '1')->orderBy('name')->get();

        return view('welcome', ['events' => $events,'news' => $news, "faculties" => $faculties]);
    }

    public function getNotice($id) {

        $notice = News::findOrFail($id);
        $news = News::where('is_published', '1')->where('is_event',NULL)->get();
        
        return view('single_notice', ['notice' => $notice, 'news' => $news]);
    }

    public function getAllNotices() {
        $news = News::where('is_published', '1')->get();

        return view('all_notices', ['news' => $news]);
    }
    
    public function getDepartment($id){
        
        $dept = \App\Department::where("is_published",1)->where("id",$id)->firstOrFail();
        return view("department",["dept"=>$dept]);
    }

    public function getFaculties($id = null) {
        $single_faculty = null;
        if (!is_null($id)) {
            $single_faculty = Faculty::where('is_published', '1')
                            ->where('id', $id)->first();
        }

        $faculties = Faculty::where('is_published', '1')->orderBy('name','asc')->get();

        return view('faculty', ['faculties' => $faculties, 'single' => $single_faculty]);
    }

   
}
