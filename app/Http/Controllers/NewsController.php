<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\News;

use Illuminate\Http\Request;

class NewsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$news = News::orderBy('created_at','desc')->get()->toJson();
		return $news;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$News = new News();
		$News->heading = str_random(10);
		$News->content = str_random(300);
		return $News->save();
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$News = new News();
		$News->heading = $request->heading;
		$News->content = $request->content;

		//return $request->heading ;
		if($News->save()){
			return json_encode(array("status"=>"success","id"=>$News->id));
		}
		return "failed";
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return News::findOrFail($id)->toJson();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,Request $request)
	{
		$News = News::findOrFail($id);
		$News->heading = $request->heading;
		$News->content = $request->content;
                $News->event_date= $request->event_date;
                $News->event_time = $request->event_time;

		//return $request->heading ;
		if($News->save()){
			return "success";
		}
		return "failed";
	}

	public function flagAsEvent($id,Request $request){
		$News = News::findOrFail($id);
		$News->is_event = $request->action;
		if($News->save()){
			return "success";
		}
		return "failed";

	}

	public function publish($id,Request $request){
		$News = News::findOrFail($id);
		$News->is_published = $request->action;
		if($News->save()){
			return "success";
		}
		return "failed";
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return News::destroy($id);
	}

}
