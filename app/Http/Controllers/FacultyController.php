<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Faculty;
use Illuminate\Http\Request;

class FacultyController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return Faculty::with("departments")->orderBy('name','asc')->get()->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        $Faculty = new Faculty();
        $Faculty->name = $request->heading;
        $Faculty->description = $request->content;

        if ($Faculty->save()) {
            return json_encode(array("status"=>"success","id"=>$Faculty->id));
        }
        return "failed";
    }

    public function publish($id,Request $request){
        $News = Faculty::findOrFail($id);
        $News->is_published = $request->action;
        if($News->save()){
            return "success";
        }
        return "failed";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        return Faculty::with("departments")->findOrFail($id)->toJson();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request) {
        
        $Faculty = Faculty::findOrFail($id);
        $Faculty->name = $request->heading;
        $Faculty->description = $request->content;

        if ($Faculty->save()) {
            return "success";
        }
        return "failed";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        return Faculty::destroy($id);
    }

}
