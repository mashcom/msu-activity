@extends('layouts.auth')
@section('content')
<div class="container-fluid login-frame" ng-controller="AuthController" >
	<div growl ttl="1000"></div>
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="alert alert-danger view-frame" ng-cloak ng-if=error_message !=undefined>
					@{{ error_message }}
			</div>
			<div class="panel panel-default">
				<div class="panel-heading"><center>Login</center></div>
				<div class="panel-body">
						

					<form class="form-horizontal" ng-submit=login() role="form" method="POST" ajjction="{{ url('/auth/login') }}">
						<input type="hidden" name="_token" ng-model="csrf_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" ng-model="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" ng-model="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" ng-model="remember" name="remember"> Remember Me
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">Login</button>

								<a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
