@include('layouts.header')

@include('layouts.nav')

 @if($single!=null)
{!! Breadcrumbs::render('single_faculty',$single) !!}
@endif
 @if($single==null)
{!! Breadcrumbs::render('faculties') !!}
 @endif

<div class="jumbotron bg-warning">
    <div class="container">
        @if($single!=null)
        <h2 style="color:#444 !important">Faculty of {{ $single->name}}</h2>
        <h4>Welcome to the faculty of {{ $single->name }}, if you need more details you can contact us</h4>
        @endif

        @if($single==null)
        <h2 style="color:#444 !important">Faculties</h2>
        <p>Welcome, select the faculty you re interested in if you need more details you can contact us</p>
        @endif
    </div>
</div>

<!-- Our Blog Section -->
@if($single !=null)
<div class="blog py-4 col-lg-12" style="background:#fff;">
    <div class="col-lg-10 col-lg-offset-1">

        <div class="container">
            <div class="py-4">

                <div class="blog-post">
                    <div class="col-lg-8">
                        <h2 class="blog-post-title">{{$single->name}}</h2>
                        {!! $single->description !!}
                    </div>

                    <div class="col-lg-4">
                        <h3>{{ $single->name }} Departments</h3>

                        @if($single->departments->count()==0)

                            <img src={{ secure_asset("/images/icons/144/advertising-144.png")}} alt="no departments"/>
                            <h4>No Departments Found</h4>

                        @endif
                        <ul class="list-group">
                            @foreach($single->departments as $dept)
                            <li class="list-group-item"><a href="{{ url('faculty/dept',[$dept->id])}}">{{ $dept->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
@endif
<!-- / Our Blog Section -->


@include('layouts.faculties')


@include('layouts.footer')

