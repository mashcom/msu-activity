@include('layouts.header')

@include('layouts.nav')

{!! Breadcrumbs::render('department',$dept) !!}

<div class="jumbotron bg-warning">
    <div class="container">
        <h2 style="color:#444 !important">Department of {{ $dept->name}}</h2>
        <p>Welcome to the department of {{ $dept->name }}, if you need more details you can contact us</p>
     
    </div>
</div>

<!-- Our Blog Section -->
<div class="blog py-4">
    <div class="col-lg-10 col-lg-offset-1">

        <div class="container">
            <div class="py-4">

                <div class="blog-post">
                    <div class="col-lg-8">
                        <h2 class="blog-post-title">{{$dept->name}}</h2>
                        {!! $dept->description !!}
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<!-- / Our Blog Section -->




@include('layouts.footer')

