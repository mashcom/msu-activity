<!-- Welcome Section -->

      <div class="welcome d-flex justify-content-center flex-column">
        <div class="container">
        </div> <!-- .container -->

        <!-- Inner Wrapper -->
        <div class="inner-wrapper mt-auto mb-auto container">
          <div class="row">
            <div class="col-md-12">
                <h1 class="welcome-heading display-4 text-white">Welcome to Midlands State University</h1>
                <p class="text-white">MSU is a development-orientated and stakeholder–driven university that is committed to the internationalization of higher education, empowerment of society and creation of wealth through relevant and actionable quality research, training, teaching as well as community engagement programmes. The university promotes gender equality and equity in student admissions, staff recruitment and promotions.</p>
                <a href="{{ url('faculties') }}" class="btn btn-lg btn-outline-white btn-pill align-self-center">Learn More</a>
            </div>
          </div>
        </div>
        <!-- / Inner Wrapper -->
      </div>
      <!-- / Welcome Section -->
