<nav class="navbar navbar-static-top navbar-dark bg-inverse">
      <a class="navbar-brand" href="{{url('/')}}">
        <img src="{{ asset('images/msu_LOGO.jpg')}}" alt="Midlands State University">
       
      </a>
      <ul class="nav navbar-nav navbar-right">
        <li class="nav-item active">
          <a class="nav-link" href="{{url('/')}}">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{url('/notice')}}">Notices</a>
        </li>
       
        <li class="nav-item">
          <a class="nav-link" href="{{url('/faculties')}}">Faculties</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="{{url('/portal')}}">Portal</a>
        </li>
      </ul>
    </nav>

   