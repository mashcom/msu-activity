<div class="testimonials section-invert section py-4">
          <h1 class="section-title text-center m-5 h1">Faculties</h1>
          <p class="text-bold h3 text-center">Choose any faculty below for more details</p>
          <br/>
          <div class="container py-5">

            @foreach($faculties->chunk(3) as $chunk)
            <div class="row" style="margin-top:20px">
            @foreach($chunk as $faculty)
            <div class="card-deck">
              <div class="col-md-12 col-lg-4">
                <div class="card  mb-4" style="border:solid 1px #dddddd !important">
                  
                  <?php
                    $icon="Midlands-State-University-360x207.jpg";
                        switch (strtolower($faculty->name)) {
                          case 'medicine':
                             $icon = "medicine-768x384.jpg";
                            break;
                           case 'commerce':
                             $icon = "Online-Ecommerce-Customer-Small-300x172.png";
                            break;

                             case 'education':
                             $icon = "education-300x121.jpg";
                            break;

                             case 'law':
                             $icon = "Law-768x384.jpg";
                            break;

                             case 'arts':
                             $icon = "arts-1024x427.jpg";
                            break;

                            case 'mining and mineral processing engineering':
                             $icon = "cement-plant-768x414.jpg";
                            break;

                            case 'natural resources management and agriculture':
                             $icon = "agric-360x108.jpg";
                            break;

                            case 'science & technology':
                             $icon = "science-768x330.jpg";
                            break;

                            case 'social sciences':
                             $icon = "Social-Science-768x452.jpg";
                            break;
                            
                          default:
                            # code...
                            break;
                        }
                    
                  ?>
                  <center>
                   <img class="card-img-top" style="height: 156px;min-height: 140px;" src="<?php  echo 'https://localhost/wordpress/wp-content/uploads/2018/01/'.$icon ?>" alt="Card image cap">
                 </center>


                  <div class="card-body">
                    <h4 class="card-title h3 text-center">{{$faculty->name}}</h4>
                    <p class="card-text"></p>
                    <center>
                    <a class="btn btn-primary text-bold text-center btn-pill" href="{{ url('faculties',[$faculty->id]) }}">Read More →</a>
                  </center>
                  </div>
                </div>
              </div>

              </div>

              @endforeach

              
            </div>
            @endforeach
          </div>

             <br/><br/><br/> 
      </div>
