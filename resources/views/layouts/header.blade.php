<html class="no-js" lang="en"><head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Midlands State University</title>
        <meta name="description" content="A demo landing page for agencies or product oriented businesses built using Shards, a free, modern and lightweight UI toolkit based on Bootstrap 4.">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSS Dependencies -->
        <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" =>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" >
        <link rel="stylesheet" href="{{asset('css/shards.css')}}">
        <link rel="stylesheet" href="{{asset('css/shards-extras.css')}}">
    </head>
    <body class="shards-landing-page--1">