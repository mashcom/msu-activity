
<!-- Our Blog Section -->
<div class="blog section section-invert py-4">
    <h1 class="section-title h1 text-center m-5">Latest News</h1>

    <div class="container">

        <div class="py-4">
           @foreach ($news->chunk(3) as $chunk)
            <div class="row" style="margin-top:20px">
                @foreach ($chunk as $news_article)
                <div class="card-deck">
                    <div class="col-md-12 col-lg-4">
                        <div class="card mb-4">
                            <!--<img class="card-img-top" src="{{ asset('images/common/card-2.jpg') }}" alt="Card image cap">
                            --><div class="card-body">
                                <h4 class="card-title">{{ $news_article->heading }}</h4>
                                <p class="card-text">Posted {{ $news_article->created_at->diffForHumans() }}</p>
                                <a class="btn btn-primary btn-pill" href="{{url('/notice',[$news_article->id]) }}">Read More →</a>
                            </div>
                        </div>
                    </div>


                </div>
                @endforeach
            </div>
             @endforeach
        </div>
    </div>

    <div class="container">
        <center>
            <br/><br/>
            <a href="{{ url('notice') }}"type="button" class="btn btn-warning text-bold btn-pill btn-lg">
                More News & Notices
            </a>
            <br/><br/>
        </center>

    </div>
</div>
<!-- / Our Blog Section -->