<!DOCTYPE html>
<html lang="en" ng-app="PortalApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Midlands State University</title>

    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">

     <link rel="stylesheet" href="{{ asset('dist/css/adminLTE.css') }}"/>
     <link rel="stylesheet" href="{{ asset('/css/angular-growl.min.css') }}">
     <!-- loadingbar -->
        <link rel='stylesheet' href="{{ asset('js/vendor/cfploader/loading-bar.css') }}"/>
        <link href="{{ asset('/css/auth.css') }}" rel="stylesheet">
    <!-- Fonts -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="auth-page">
<br/>
<br/><br/>
<div class="col-lg-12 text-center">
  
    <h1 class="text-bold">Welcome to MSU Portal</h1>
</div>
@yield('content')

<div class="col-lg-12 text-center text-bold">
    <h6>Midlands State University &copy; 2018</h6>
     <h6>Powered by Mashcom</h6>
</div>

</body>

 <!-- angular -->
        <script src="{{ asset('angular/angular.min.js') }}"></script>
        <script src="{{ asset('angular/angular-route.min.js') }}"></script>
        <script src="{{ asset('angular/angular-sanitize.min.js') }}"></script>
        <script src="{{ asset('angular/angular-resource.min.js') }}"></script>
        <script src="{{ asset('angular/angular-animate.min.js') }}"></script>

        <script src="{{ asset('angular/app/app.js') }}"></script>
        <script src="{{ asset('angular/app/router.js') }}"></script>
        <script src="{{ asset('angular/app/angular-growl.min.js') }}"></script>

        <script src="{{ asset('js/vendor/cfploader/app.js') }}"></script>
</html>
