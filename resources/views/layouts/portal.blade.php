<!DOCTYPE html>
<html lang="en" ng-app="PortalApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MSU Portal</title>

    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
     <link rel="stylesheet" href="{{ asset('dist/css/adminLTE.css') }}"/>
   
    <link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- daterange picker -->
   <link rel="stylesheet" href="{{ asset('/plugins/daterangepicker/daterangepicker-bs3.css') }}">
   <!-- iCheck for checkboxes and radio inputs -->
   <link rel="stylesheet" href="{{ asset('/plugins/iCheck/all.css') }}">
   <!-- Bootstrap Color Picker -->
   <link rel="stylesheet" href="{{ asset('/plugins/colorpicker/bootstrap-colorpicker.min.css') }}">
   <!-- Bootstrap time Picker -->
   <link rel="stylesheet" href="{{ asset('/plugins/timepicker/bootstrap-timepicker.min.css') }}">
   <!-- Select2 -->
   <link rel="stylesheet" href="{{ asset('/plugins/select2/select2.min.css') }}">

    <link rel="stylesheet" href="{{ asset('/css/angular-growl.min.css') }}">
     <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css') }}"/>
     <!-- loadingbar -->
        <link rel='stylesheet' href="{{ asset('js/vendor/cfploader/loading-bar.css') }}"/>

         <link rel="stylesheet" href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <!-- Fonts -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-yellow sidebar-mini fixed sidebar-collapse">

    <nav class="navbar navbar-inverse navbar-fixed-top" style='border-radius:0' role="navigation">

        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand logo text-" href="#/">  <img src="{{ asset('images/msu_logo_white.jpg')}}" alt="Midlands State University">
       MSU Portal</a>
            </div>

            <ul class="nav navbar-nav navbar-right navbar-collapse collapse">
                    @if(Auth::user())

                        <li><a href="#/faculty" class="">Faculties</a></li>
                        <li><a href="#/posts" class="">News</a></li>
                        <li><a href="{{ url('/') }}" class="">Website</a></li>
                        <li><a>Hello! <b>{{Auth::user()->name}}</b></a></li>
                        <li><a href="{{url('auth/logout')}}" class="">Sign out</a></li>
                    @endif


            </ul>
    </nav>

    <div class="container">
        <div growl ttl="1000"></div>
        <div ng-view class="view-frame"></div>
      </div>

    <footer class="footer">
      <div class="container">
       <h6 class="text-center">Midlands State University &copy; 2018</h6>
       <h6 class="text-center">Developed by Mashcom</h6>
      </div>
    </footer>

      <!-- angular -->
        <script src="{{ asset('angular/angular.min.js') }}"></script>
        <script src="{{ asset('angular/angular-route.min.js') }}"></script>
        <script src="{{ asset('angular/angular-sanitize.min.js') }}"></script>
        <script src="{{ asset('angular/angular-resource.min.js') }}"></script>
        <script src="{{ asset('angular/angular-animate.min.js') }}"></script>

        <script src="{{ asset('angular/app/app.js') }}"></script>
        <script src="{{ asset('angular/app/router.js') }}"></script>
        <script src="{{ asset('angular/app/angular-growl.min.js') }}"></script>

        <script src="{{ asset('js/vendor/cfploader/app.js') }}"></script>

    <!-- UI Scripts -->
    <script type="text/javascript" src="{{ asset('/js/jQuery-2.1.4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
    
    <script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>

    <script type="text/javascript">
       function openModal(element) {
                    $(element).modal();
                }
                function closeModal(element) {
                    $(element).modal("hide");
                }


    </script>

</body>
</html>
