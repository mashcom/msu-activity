  <div class="jumbotron bg-warning">
      <div class="container">
        <h3 style="color:#444 !important">News & Notices</h3>
      </div>
    </div>
      <!-- Our Blog Section -->
      <div class="blog py-4 col-lg-12">

        <div class="container ">

          <div class="py-4 col-lg-10 col-lg-offset-1">
           
           <div class="blog-post">
            <h2 class="blog-post-title">{{$notice->heading}}</h2>
            <p class="blog-post-meta h3"><span class="label label-primary">Posted {{ $notice->created_at->diffForHumans() }}</span></p>
            <div class="blog-content">
            {!! $notice->content !!}
          </div>

            </div>

            

            
          </div>
        
        </div>

      </div>
      <!-- / Our Blog Section -->