<?php

// Home
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('home'));
});

// Home > Notices
Breadcrumbs::register('notice', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('News & Notices', route('notice'));
});

// Home > News & Notices > [Article]
Breadcrumbs::register('single_notice', function($breadcrumbs, $notice)
{
    $breadcrumbs->parent('notice');
    $breadcrumbs->push($notice->heading, route('notice', $notice->id));
});

// Home > Faculties
Breadcrumbs::register('faculties', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Faculties', route('faculties'));
});

// Home > Faculties > [Faculty]
Breadcrumbs::register('single_faculty', function($breadcrumbs, $faculty)
{
    $breadcrumbs->parent('faculties');
    $breadcrumbs->push($faculty->name, route('faculties', $faculty->id));
});

// Home > Faculties > [Faculty] > [Department]
Breadcrumbs::register('department', function($breadcrumbs, $dept)
{
    $breadcrumbs->parent('single_faculty',$dept->faculty);
    $breadcrumbs->push($dept->name, route('department', $dept->id));
});


